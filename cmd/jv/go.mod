module github.com/santhosh-tekuri/jsonschema/cmd/jv

go 1.21.1

require (
	gitee.com/jack1995/jsonschema v0.0.2
	github.com/spf13/pflag v1.0.5
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/dlclark/regexp2 v1.11.4 // indirect
	golang.org/x/text v0.14.0 // indirect
)

// replace gitlab.supfusion.supos.com/supos-kernel/jsonschema v6.0.1 => ../..

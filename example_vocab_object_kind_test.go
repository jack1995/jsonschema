package jsonschema_test

import (
	"errors"
	"fmt"
	jsonschema "gitee.com/jack1995/jsonschema"
	"log"
	"reflect"
	"strings"
	"testing"
)

// SchemaExt --

type objectKind struct {
	key    string
	values []string
}

func (d *objectKind) Validate(ctx *jsonschema.ValidatorContext, v any) {
	fmt.Printf("\n  ext :%v", ctx.ExtMap)
	fmt.Printf("\n  kind :%s , values : %v, obj : %v ", d.key, d.values, v)
}

// Vocab --
func ObjectKindVocab() *jsonschema.Vocabulary {
	url := "http://example.com/meta/objectKind"
	schema, err := jsonschema.UnmarshalJSON(strings.NewReader(`{
		"properties": {
			"$object-reference-kind": { 	
				 "type": "array",
 				 "items": {
    				"type": "string"
				} 
			}
		}
	}`))
	if err != nil {
		log.Fatal(err)
	}

	c := jsonschema.NewCompiler()
	if err := c.AddResource(url, schema); err != nil {
		log.Fatal(err)
	}
	sch, err := c.Compile(url)
	if err != nil {
		log.Fatal(err)
	}

	return &jsonschema.Vocabulary{
		URL:     url,
		Schema:  sch,
		Compile: compileObjectKind,
	}
}

func compileObjectKind(ctx *jsonschema.CompilerContext, obj map[string]any) (jsonschema.SchemaExt, error) {
	var keyword = "$object-reference-kind"
	value, exit := obj[keyword]
	if !exit {
		return nil, nil
	}

	slice, ok := value.([]any)
	if !ok {
		return nil, errors.New("input is not a slice")
	}
	var values []string
	// 检查切片中的每个元素是否是字符串
	for _, v := range slice {
		if reflect.TypeOf(v).Kind() != reflect.String {
			return nil, errors.New("slice contains non-string elements")
		}
		values = append(values, v.(string))
	}
	return &objectKind{values: values, key: keyword}, nil
}

// Example --

func Test_vocab_ObjectKind(t *testing.T) {

	schema, err := jsonschema.UnmarshalJSON(strings.NewReader(`{
		"type": "object",
		"properties": {
			"kind": { "type": "string" },
			"fish": {
				"type": "object",
				"properties": {
					"swimmingSpeed": { 
						"type": "number"
						
					}
				},
				"required": ["swimmingSpeed"],
				"$object-reference-kind": ["111111"]
			},
			"dog": {
				"type": "object",
				"properties": {
					"runningSpeed": { 
						"type": "number" 
					}
				},
				"required": ["runningSpeed"],
				"$object-reference-kind": ["22222"]
			}
		},
		"required": ["kind"]
	}`))
	if err != nil {
		fmt.Println("xxx", err)
		log.Fatal(err)
	}
	inst, err := jsonschema.UnmarshalJSON(strings.NewReader(`{
		"kind": "fish",
		"dog":{"runningSpeed": 5},
		"fish":{"swimmingSpeed": 3}
	}`))
	if err != nil {
		log.Fatal(err)
	}
	c := jsonschema.NewCompiler()
	c.AssertVocabs()
	c.RegisterVocabulary(ObjectKindVocab())
	if err := c.AddResource("schema.json", schema); err != nil {
		log.Fatal(err)
	}
	sch, err := c.Compile("schema.json")
	if err != nil {
		log.Fatal(err)
	}

	var aa = make(map[string]any)
	aa["111"] = "456"
	aa["222"] = "789"
	aa["333"] = "101112"
	err = sch.ValidateAndExt(inst, aa)
	fmt.Println("valid:", err == nil)
	// Output:
	// valid: false
}
